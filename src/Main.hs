{-# LANGUAGE UnicodeSyntax #-}

module Main where

import Options.Applicative
import Data.Semigroup ((<>))

data Sample = Sample
  { hello :: String
  , quiet :: Bool
  , enthusiasm :: Int
  , show :: String
  , svalue :: String
  }

sample :: Parser Sample
sample = Sample
  <$> strOption
     ( long "hello"
    <> metavar "TARGET"
    <> help "Target for the greeting" )
  <*> switch
     ( long "quiet"
    <> short 'q'
    <> help "Whethr to be quiet" )
  <*> option auto
     ( long "enthusiasm"
    <> help "How enthusiastically to greet"
    <> showDefault
    <> value 1
    <> metavar "INT" )
  <*> strOption
    (   long "show"
    <> short 's'
    <> metavar "STATUS"
    <> help "Status of the issue/PR, can be: open, closed, merged." )
  <*> strOption
    (   long "svalue"
    <> metavar "VALUE"
    <> value "default"
    <> help "Override default name" )

greet :: Sample -> IO ()
greet (Sample h False n s v) = putStrLn $ "Hello, " <> h <> replicate n '!' <> s <> v
greet _ = return ()

main ∷ IO ()
main = do
  greet =<< execParser opts
   where
    opts = info (sample <**> helper)
       ( fullDesc
      <> progDesc "Print a greeting for TARGET"
      <> header "hello - a test for optparse-applicative")

  -- putStrLn "done!"
