{ mkDerivation, base, base-unicode-symbols, optparse-applicative
, stdenv
}:
mkDerivation {
  pname = "spinner";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    base base-unicode-symbols optparse-applicative
  ];
  homepage = "http://haskellbook.com/";
  description = "Trivial CLI program";
  license = stdenv.lib.licenses.bsd3;
}
